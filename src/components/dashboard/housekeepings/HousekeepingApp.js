import React from "react";
import { Route, Routes } from "react-router-dom";
import HousekeepingCreate from "./HousekeepingCreate";
import HousekeepingIndex from "./HousekeepingIndex";

const HousekeepingApp = () => {
  return (
    <Routes>
      <Route path="/" element={<HousekeepingIndex />} />
      <Route path="/create" element={<HousekeepingCreate />} />
    </Routes>
  );
};

export default HousekeepingApp;
