import React from "react";
import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

import axiosInstance from "../../../helpers/instance";
const Billing = () => {
  const token = localStorage.getItem("token");
    let {userId} = useParams();
    const [billing, setBilling] = useState(null);

  const print = () => {
    window.print();
  }

  useEffect(()=>{
    axiosInstance.get(`calculate/${userId}`).then(res=>{
        setBilling(res.data)
    })
  }, []);

    return (
    <div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-header">
              <div className="card-title text-lg text-bold text-indigo mb-2">Billing</div>
              <div className="card-tools">
                <Link to="/dashboard/bookings" className="btn-sm bg-indigo mr-4">
                  <i className="fa fa-arrow-left mr-1" aria-hidden="true"></i>{" "}
                  Go back
                </Link>
              </div>
            </div>
            <div className="card-body p-0" style={{ overflowX: "auto" }}>
                <table className="table table-hover table-bordered">
                    <tr>
                        <td className="text-blue">User Name</td>
                        <td className="text-primary">{billing?.user.name}</td>
                    </tr>
                    <tr>
                        <td className="text-blue">Room Amount</td>
                        <td className="text-red">Rs. {billing?.room}</td>
                    </tr>
                    <tr>
                        <td className="text-blue">Food Amount</td>
                        <td className="text-red">Rs. {billing?.food}</td>
                    </tr>
                    <tr>
                        <td className="text-blue">Subtotal</td>
                        <td className="text-red">Rs. {billing?.subtotal}</td>
                    </tr>
                    <tr>
                        <td className="text-purple">User Points (You must have 2000 or more points to get a discount)</td>
                        <td className="text-purple">{billing?.user.points}</td>
                    </tr>
                    <tr>
                        <td className="text-success">Discount</td>
                        <td className="text-success">Rs. {billing?.discount}</td>
                    </tr>
                    <tr>
                        <td className="text-lg text-red text-bold">Total</td>
                        <td className=" text-lg text-red text-bold ">Rs. {billing?.total}</td>
                    </tr>
                </table>
                <button onClick={print}  className="btn-primary btn float-right mr-8 mt-3 mb-3 ">
                    <p className="m-2 text-bold">Print</p>
                </button>
                <button className="btn-success btn float-right mr-8 mt-3 mb-3 ">
                    <p className="m-2 text-bold"><a href="https://esewa.com.np/#/home" target="_blank" className="text-white">Pay with Esewa</a></p>
                </button>
                
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Billing;
