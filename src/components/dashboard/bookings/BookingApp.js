import React from "react";
import { Route, Routes } from "react-router-dom";
import BookingCreate from "./BookingCreate";
import BookingEdit from "./BookingEdit";
import BookingIndex from "./BookingIndex";
import Billing from "./Billing";

const BookingApp = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<BookingIndex />} />
        <Route path="/create" element={<BookingCreate />} />
        <Route path="/edit/:id" element={<BookingEdit />} />
        <Route path="/billing/:userId" element={<Billing />} />
      </Routes>
    </div>
  );
};

export default BookingApp;
