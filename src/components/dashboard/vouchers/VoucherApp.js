import React from "react";
import { Route, Routes } from "react-router-dom";
import VoucherCreate from "./VoucherCreate";
import VoucherEdit from "./VoucherEdit";
import VoucherIndex from "./VoucherIndex";
import VoucherShow from "./VoucherShow";
import VoucherPrev from "./VoucherPrev";

const VoucherApp = () => {
  return (
    <Routes>
      <Route path="/" element={<VoucherIndex />} />
      <Route path="/create" element={<VoucherCreate />} />
      <Route path="/prev" element={<VoucherPrev />} />
      <Route exact path="/edit/:id" element={<VoucherEdit />} />
      <Route exact path="/:id" element={<VoucherShow />} />
    </Routes>
  );
};

export default VoucherApp;
