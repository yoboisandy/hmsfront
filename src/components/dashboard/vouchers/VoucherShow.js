import React from "react";
import axiosInstance from "../../../helpers/instance";
import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";

const VoucherShow = () => {
  const [voucherData, setVoucherData] = useState({});
  const [loading, setLoading] = useState(false);
  let { id } = useParams();
  const token = localStorage.getItem("token");

  const fetchVoucher = async () => {
    setLoading(true);
    await axiosInstance
      .get(`http://localhost:8000/api/vouchers/${id}`)
      .then((res) => {
        setVoucherData(res.data);
      });
    setLoading(false);
    console.log(voucherData);
  };

  useEffect(() => {
    fetchVoucher();
  }, []);

  return (
    <div>
      <div className="card">
        <div className="card-header">
          <div className="card-title text-lg">Voucher Detail</div>

          <div className="card-tools">
            <Link
              to={`/dashboard/vouchers/edit/${id}`}
              className="btn-sm bg-teal mr-1"
            >
              <i className=" fas fa-edit mr-1"></i>Edit
            </Link>
            <Link to="/dashboard/vouchers" className="btn-sm bg-indigo">
              <i className="fa fa-arrow-left mr-1" aria-hidden="true"></i> Go
              back
            </Link>
          </div>
        </div>
        <div className="card-body p-0">
          <table className="table table-bordered">
            <tbody>
              {loading ? (
                <tr>
                  <td colSpan={9}>
                    <div className="d-flex justify-content-center py-5">
                      <div className="spinner-border text-indigo" role="status">
                        <span className="sr-only">Loading...</span>
                      </div>
                    </div>
                  </td>
                </tr>
              ) : (
                <>
                  <tr>
                    <th>SN</th>
                    <td>{voucherData.id}</td>
                  </tr>
                  <tr>
                    <th>Name</th>
                    <td>{voucherData.name}</td>
                  </tr>
                </>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default VoucherShow;
