import React from "react";
import { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import axiosInstance from "../../../helpers/instance";

const VoucherCreate = () => {
  const [validationErr, setValidationErr] = useState({});
  const navigate = useNavigate();
  const [voucherData, setVoucherData] = useState({});
  const [loading, setLoading] = useState(false);
  const token = localStorage.getItem("token");

  const handleInputChange = (e) => {
    setVoucherData({ ...voucherData, [e.target.name]: e.target.value });
    console.log(voucherData);
  };

  const saveVoucher = async (e) => {
    e.preventDefault();

    setLoading(true);
    const fd = new FormData();
    fd.append("code", voucherData.code);
    fd.append("discount_percentage", voucherData.discount_percentage);
    fd.append("valid_from", voucherData.valid_from);
    fd.append("valid_until", voucherData.valid_until);
    fd.append("roomtype_id", voucherData.roomtype_id);

    await axiosInstance
      .post("http://localhost:8000/api/vouchers", fd)
      .then((res) => {
        Swal.fire({
          position: "center",
          icon: "success",
          title: res.data.message,
          showConfirmButton: false,
          timer: 2000,
        });
        navigate("/dashboard/vouchers");
      })
      .catch((err) => {
        setValidationErr(err.response.data.errors);
      });
    setLoading(false);
  };

  return (
    <div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-header">
              <div className="card-title text-lg">Add Voucher</div>
              <div className="card-tools">
                <Link to="/dashboard/vouchers" className="btn-sm bg-indigo">
                  <i className="fa fa-arrow-left mr-1" aria-hidden="true"></i>{" "}
                  Go back
                </Link>
              </div>
            </div>
            <div className="card-body ">
              <form onSubmit={saveVoucher} method="post">
                <div className="form-group">
                  <label htmlFor="code">Voucher Code</label>
                  <input
                    onChange={handleInputChange}
                    value={voucherData.code}
                    name="code"
                    type="text"
                    className={`form-control ${
                      validationErr.code ? "is-invalid" : ""
                    }`}
                    id="code"
                    placeholder="Enter Voucher Code"
                  />
                  {validationErr.code ? (
                    <>
                      <span className="text-danger form-text">
                        {validationErr.code}
                      </span>
                    </>
                  ) : (
                    ""
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="discount_percentage">Discount Percentage</label>
                  <input
                    onChange={handleInputChange}
                    value={voucherData.discount_percentage}
                    name="discount_percentage"
                    type="text"
                    className={`form-control ${
                      validationErr.discount_percentage ? "is-invalid" : ""
                    }`}
                    id="discount_percentage"
                    placeholder="Enter Discount Percentage"
                  />
                  {validationErr.discount_percentage ? (
                    <>
                      <span className="text-danger form-text">
                        {validationErr.discount_percentage}
                      </span>
                    </>
                  ) : (
                    ""
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="roomtype_id">Room Type</label>
                  <input
                    onChange={handleInputChange}
                    value={voucherData.roomtype_id}
                    name="roomtype_id"
                    type="text"
                    className={`form-control ${
                      validationErr.roomtype_id ? "is-invalid" : ""
                    }`}
                    id="roomtype_id"
                    placeholder="Enter Room Type"
                  />
                  {validationErr.roomtype_id ? (
                    <>
                      <span className="text-danger form-text">
                        {validationErr.roomtype_id}
                      </span>
                    </>
                  ) : (
                    ""
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="valid_from">Valid From</label>
                  <input
                    onChange={handleInputChange}
                    value={voucherData.valid_from}
                    name="valid_from"
                    type="date"
                    className={`form-control ${
                      validationErr.valid_from ? "is-invalid" : ""
                    }`}
                    id="valid_from"
                  />
                  {validationErr.valid_from ? (
                    <>
                      <span className="text-danger form-text">
                        {validationErr.valid_from}
                      </span>
                    </>
                  ) : (
                    ""
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="valid_until">Valid Until</label>
                  <input
                    onChange={handleInputChange}
                    value={voucherData.valid_until}
                    name="valid_until"
                    type="date"
                    className={`form-control ${
                      validationErr.valid_until ? "is-invalid" : ""
                    }`}
                    id="valid_until"
                  />
                  {validationErr.valid_until ? (
                    <>
                      <span className="text-danger form-text">
                        {validationErr.valid_until}
                      </span>
                    </>
                  ) : (
                    ""
                  )}
                </div>
                

                <div className="form-group my-2">
                  <button
                    onClick={saveVoucher}
                    type="submit"
                    className="btn bg-indigo"
                  >
                    {loading ? (
                      <>
                        <span
                          className="spinner-border spinner-border-sm mr-2"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        <span>Saving...</span>
                      </>
                    ) : (
                      "Create"
                    )}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VoucherCreate;
