import React from "react";
import { useEffect, useState } from "react";
import axiosInstance from "../../../helpers/instance";
import Swal from "sweetalert2";
import { Link, useNavigate, useParams } from "react-router-dom";

const VoucherEdit = () => {
  const [voucherData, setVoucherData] = useState({});
  const [loading, setLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [validationErr, setValidationErr] = useState({});
  const navigate = useNavigate();
  const token = localStorage.getItem("token");

  const handleInputChange = (e) => {
    setVoucherData({ ...voucherData, [e.target.name]: e.target.value });
    console.log(voucherData);
  };

  let { id } = useParams();

  const updateVoucher = async (e) => {
    e.preventDefault();
    setBtnLoading(true);
    try{
    await axiosInstance
      .put(`http://localhost:8000/api/vouchers/${id}`, voucherData);
        Swal.fire({
          position: "center",
          icon: "success",
          title: "voucher updated sucessfully",
          showConfirmButton: false,
          timer: 2000,
        });
        console.log(voucherData)
        navigate("/dashboard/vouchers");
      }catch(error) {
        if(error.response){
          setValidationErr(error.response.data.errors);
        }
      }
    setBtnLoading(false);
  };

  const fetchVoucher = async () => {
    setLoading(true);
    try{
      const response =
    await axiosInstance
      .get(`http://localhost:8000/api/vouchers/${id}`);
      
      setVoucherData({
          status: response.data.status,
        });
      }catch(error){
        console.log("Error fetching voucher:", error);
      }
    setLoading(false);
    // console.log(voucherData);
  };

  useEffect(() => {
    fetchVoucher();
  }, [id]);

  const statusOptions = ["Enable","Expired"];
  return (
    <div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-header">
              <div className="card-title text-lg">Update Voucher</div>
              <div className="card-tools">
                <Link to="/dashboard/vouchers" className="btn-sm bg-indigo">
                  <i className="fa fa-arrow-left mr-1" aria-hidden="true"></i>{" "}
                  Go back
                </Link>
              </div>
            </div>
            <div className="card-body ">
              {loading ? (
                <div className="d-flex justify-content-center py-5">
                  <div className="spinner-border text-indigo" role="status">
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>
              ) : (
                <form onSubmit={updateVoucher} method="post">
                  <div className="form-group">
                    <label htmlFor="status">Status</label>
                    <select
                      onChange={handleInputChange}
                      name="status"
                      value={voucherData.status || ""}
                      className={`form-control ${validationErr.status ? "is-invalid" : ""}`}
                      id="status"
                    >
                      <option value="" disabled>Select Status</option>
                      {statusOptions.map((option) => (
                        <option key={option} value={option}>
                          {option}
                        </option>
                      ))}
                    </select>
                    {validationErr.status ? (
                      <>
                        <span className="text-danger form-text">
                          {validationErr.status}
                        </span>
                      </>
                    ) : (
                      ""
                    )}
                  </div>

                  <div className="form-group my-2">
                    <button
                      onClick={updateVoucher}
                      type="submit"
                      className="btn bg-indigo"
                    >
                      {btnLoading ? (
                        <>
                          <span
                            className="spinner-border spinner-border-sm mr-2"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          <span>Updating...</span>
                        </>
                      ) : (
                        "Update"
                      )}
                    </button>
                  </div>
                </form>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VoucherEdit;
