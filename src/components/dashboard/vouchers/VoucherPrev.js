import React from "react";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

import axiosInstance from "../../../helpers/instance";
const VoucherPrev = () => {
  const [vouchers, setVouchers] = useState([]);
  const [loading, setLoading] = useState(false);
  const token = localStorage.getItem("token");

  const fetchVouchers = async () => {
    setLoading(true);
    await axiosInstance
      .get("http://localhost:8000/api/vouchers/prev")
      .then((res) => {
        setVouchers(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err.response.data.msg);
      });
    setLoading(false);
  };

  useEffect(() => {
    fetchVouchers();
  }, []);

  return (
    <div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-header">
              <div className="card-title text-lg">Expired Vouchers</div>
              <div className="card-tools mr-1">
                <Link
                  to="/dashboard/vouchers"
                  className="btn-sm bg-primary"
                >
                  <i className="fas fa-arrow-left mr-1"></i> Go Back
                </Link>
              </div>
            </div>
            <div className="card-body p-0" style={{ overflowX: "auto" }}>
              <table className="table table-hover table-bordered">
                <thead className="bg-indigo">
                  <tr className="text-center">
                    <th>SN</th>
                    <th>Code</th>
                    <th>Discount</th>
                    <th>Room Type</th>
                    <th>Valid From</th>
                    <th>Valid Until</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  {loading ? (
                    <tr>
                      <td colSpan={9}>
                        <div className="d-flex justify-content-center py-5">
                          <div
                            className="spinner-border text-indigo"
                            role="status"
                          >
                            <span className="sr-only">Loading...</span>
                          </div>
                        </div>
                      </td>
                    </tr>
                  ) : (
                    ""
                  )}
                  {vouchers.map((voucher, index) => {
                    return (
                      <tr>
                        <td>{index + 1}</td>
                        <td>{voucher.code}</td>
                        <td>{voucher.discount_percentage}  %</td>
                        <td>{voucher.roomtype?.type_name}</td>
                        <td>{voucher.valid_from}</td>
                        <td>{voucher.valid_until}</td>
                        <td>{voucher.status}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VoucherPrev;
