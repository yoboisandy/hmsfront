import React from "react";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

import axiosInstance from "../../../helpers/instance";
const VoucherIndex = () => {
  const [vouchers, setVouchers] = useState([]);
  const [loading, setLoading] = useState(false);
  const token = localStorage.getItem("token");

  const fetchVouchers = async () => {
    setLoading(true);
    await axiosInstance
      .get("http://localhost:8000/api/vouchers")
      .then((res) => {
        setVouchers(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err.response.data.msg);
      });
    setLoading(false);
  };

  useEffect(() => {
    fetchVouchers();
  }, []);

  const handleDelete = async (id) => {
    const isConfirmed = await Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((res) => {
      return res.isConfirmed;
    });

    if (isConfirmed) {
      await axiosInstance
        .delete(`http://localhost:8000/api/vouchers/${id}`)
        .then((res) => {
          Swal.fire({
            icon: "success",
            text: res.data.message,
          });
          fetchVouchers();
        })
        .catch((err) => {
          if (err.response.status === 500) {
            Swal.fire({
              text: "Status " + err.response.status + ": Something went wrong!",
              icon: "error",
            });
          } else {
            Swal.fire({
              text: "Status " + err.response.status + ": Something went wrong!",
              icon: "error",
            });
          }
        });
    }
  };

  return (
    <div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-header">
              <div className="card-title text-lg">Vouchers</div>
              <div className="card-tools ml-2">
                <Link
                  to="/dashboard/vouchers/prev"
                  className="btn-sm bg-danger"
                >
                  <i className="fas fa-minus-circle mr-1"></i>Expired Vouchers</Link>
              </div>

              <div className="card-tools mr-1">
                <Link
                  to="/dashboard/vouchers/create"
                  className="btn-sm bg-success"
                >
                  <i className="fas fa-plus-circle mr-1"></i> Add New
                </Link>
              </div>
            </div>
            <div className="card-body p-0" style={{ overflowX: "auto" }}>
              <table className="table table-hover table-bordered">
                <thead className="bg-indigo">
                  <tr className="text-center">
                    <th>SN</th>
                    <th>Code</th>
                    <th>Discount</th>
                    <th>Room Type</th>
                    <th>Valid From</th>
                    <th>Valid Until</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {loading ? (
                    <tr>
                      <td colSpan={9}>
                        <div className="d-flex justify-content-center py-5">
                          <div
                            className="spinner-border text-indigo"
                            role="status"
                          >
                            <span className="sr-only">Loading...</span>
                          </div>
                        </div>
                      </td>
                    </tr>
                  ) : (
                    ""
                  )}
                  {vouchers.map((voucher, index) => {
                    return (
                      <tr>
                        <td>{index + 1}</td>
                        <td>{voucher.code}</td>
                        <td>{voucher.discount_percentage}  %</td>
                        <td>{voucher.roomtype?.type_name}</td>
                        <td>{voucher.valid_from}</td>
                        <td>{voucher.valid_until}</td>
                        <td>{voucher.status}</td>
                        <td className="d-flex justify-content-center">
                          {/* <Link
                            to={`/dashboard/vouchers/${voucher.id}`}
                            className="btn-sm bg-success mr-1"
                          >
                            <i className="fa fa-eye"> </i>
                          </Link> */}
                          <Link
                            to={`/dashboard/vouchers/edit/${voucher.id}`}
                            className="btn-sm bg-teal mr-1"
                          >
                            <i className=" fas fa-edit"> </i>
                          </Link>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VoucherIndex;
