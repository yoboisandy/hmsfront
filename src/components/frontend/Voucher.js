import axiosInstance from "../../helpers/instance";
import React, {useEffect, useState } from "react";
import Spinner from "./components/Spinner";

const Voucher = () => {
  const [vouchers, setVouchers] = useState([]);
  const [loading, setLoading] = useState(false);
  // const token = localStorage.getItem("token");

  const fetchVouchers = async () => {
    setLoading(true);
    await axiosInstance
      .get("http://localhost:8000/api/vouchers")
      .then((res) => {
        setVouchers(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err.response.data.msg);
      });
    setLoading(false);
  };

  useEffect(() => {
    fetchVouchers();
  }, []);

  return (
    <div>
      {loading && <Spinner />}
      {!loading && (
        <div>
          {/* <div>Your Bookings</div> */}
          <div className="bg-indigo-900 h-72 bg-cover bg-center bg-[url('https://technext.github.io/royal/image/about_banner.jpg')] bg-blend-soft-light bg-fixed flex items-center justify-center">
            <div className="text-white ">
              {/* <div className="border-l-4 border-indigo-500 w-6/12 mx-auto"></div> */}
              <h2 className="text-6xl font-semibold tracking-wider border-l-8 border-indigo-500 pl-3 text-center flex items-center">
                <span>Vouchers</span>
              </h2>
            </div>
          </div>
           <div className="mx-40 my-16 overflow-x-auto rounded shadow">
            <table className="w-full text-left">
              <thead className="bg-indigo-100 font-medium">
                <tr>
                  <td className="px-6 py-4">SN</td>
                  <td className="px-6 py-4">Code</td>
                  <td className="px-6 py-4">Discount</td>
                  <td className="px-6 py-4">Room Type</td>
                  {/* <td className="px-6 py-4">Valid From</td>
                  <td className="px-6 py-4">Valid Until</td> */}
                </tr>
              </thead>
              <tbody>
                {vouchers.map((el, index) => {
                  return (
                    <tr className="border-b p-10 hover:bg-gray-100 transition-all ease-linear">
                      <td className="px-6 py-4">{index + 1}</td>
                      <td className="px-6 py-4">{el.code}</td>
                      <td className="px-6 py-4">{el.discount_percentage} %</td>
                      <td className="px-6 py-4">{el.roomtype?.type_name} </td>
                      {/* <td className="px-6 py-4">{el.valid_from}</td>
                      <td className="px-6 py-4">{el.valid_until}</td> */}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      )}
    </div>
  );
};

export default Voucher;