import axiosInstance from "../../helpers/instance";
import React, { useEffect, useState } from "react";
import RoomCard from "./components/RoomCard";
import Spinner from "./components/Spinner";

const Rooms = () => {
  const [loading, setLoading] = useState(false);
  const [DDRoomTypes, setDDRoomTypes] = useState([]);
  const [availability, setAvailability] = useState(true);
  const [notAvailableMsg, setNotAvailableMsg] = useState("");
  const [validationErr, setValidationErr] = useState({});
  const [occupancy, setOccupancy] = useState("");
  const [searched, setSearched] = useState(false);
  const [roomTypes, setRoomTypes] = useState([
    {
      amenities: [],
    },
  ]);
  const token = localStorage.getItem("token");

  const [searchData, setSearchData] = useState({
    start_date: "",
    end_date: "",
    roomtype_id: null,
  });

  // const handleSearchRoom = (e) => {
  //   e.preventDefault();
  //   fetchRoomType();
  // };

  const fetchRoomType = async (occupancyValue) => {
    setLoading(true);
    await axiosInstance.get(`/viewroomtypes`).then((res) => {
    const parsedOccupancyValue = parseInt(occupancyValue);
      if (isNaN(parsedOccupancyValue) || parsedOccupancyValue <= 0) {
        // If occupancyValue is not a valid number or empty, show all room types
        setRoomTypes(res.data);
        setAvailability(true);
      }else{
        const filteredRoomTypes = res.data.filter(
          (roomType) => roomType.occupancy >= parsedOccupancyValue
        );
        setRoomTypes(filteredRoomTypes);
        setAvailability(filteredRoomTypes.length > 0);
      }
    });
    setLoading(false);
  };


  const ddRoomType = async () => {
    setLoading(true);
    await axiosInstance.get(`/viewroomtypes`).then((res) => {
      setDDRoomTypes(res.data);
    });
    setLoading(false);
  };

  const handleSearchRoom = (e) => {
    e.preventDefault();
    setSearched(true); // Set 'searched' to true when the search form is submitted
    fetchRoomType(occupancy); // Pass the 'occupancy' state to the 'fetchRoomType' function
  };

  useEffect(() => {
    ddRoomType();
    fetchRoomType(occupancy);
  }, []);

  return (
    <div>
      <div className="">
        <div className="bg-indigo-900 h-72 bg-cover bg-center bg-[url('https://technext.github.io/royal/image/about_banner.jpg')] bg-blend-soft-light bg-fixed flex items-center justify-center">
          <div className="text-white ">
            {/* <div className="border-l-4 border-indigo-500 w-6/12 mx-auto"></div> */}
            <h2 className="text-6xl font-semibold tracking-wider border-l-8 border-indigo-500 pl-3 text-center flex items-center">
              <span>Rooms</span>
            </h2>
          </div>
        </div>
        {/* check availabiity form */}
        <div className="bg-white relative -top-16 py-5 px-4 md:mx-44 mx-5 md:rounded-full  rounded-lg border-4 border-indigo-300 ">
          <form className="space-y-3" onSubmit={handleSearchRoom}>
            <div className="md:flex justify-center  md:gap-3">
              <div className="md:w-2/5 flex flex-col items-center mt-3">
              <label htmlFor="occupancy" className="text-bold text-indigo-700 text-lg">For how many people are you booking??</label>
              </div>
              <div className="md:w-2/5 flex flex-col">
                <input
                  className="w-full h-12 bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200  text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"
                  type="number"
                  placeholder="Enter a value here!!!"
                  name="occupancy"
                  id="occupancy"
                  onChange={(e) => setOccupancy(e.target.value)}
                  style={{ '::placeholder': { color: 'indigo' }}}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="my-8">
        <div>
          <section className="text-gray-600 body-font">
            <div className="container px-5 py-12 mx-auto">
              {loading && <Spinner />}
              {!loading && (
                <>
                  {searched && !availability && (
                    <div className="font-medium  text-center">
                      No rooms available for the selected occupancy.
                    </div>
                  )}

                  {availability && (
                    <div className="flex flex-wrap -m-4">
                      {roomTypes.map((el, index) => {
                        return <RoomCard key={index} {...el} index={index} />;
                      })}
                    </div>
                  )}
                </>
              )}
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export default Rooms;
