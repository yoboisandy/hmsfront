import axiosInstance from "../../helpers/instance";
import React, { useEffect, useState } from "react";
import HallCard from "./components/HallCard";
import RoomCard from "./components/RoomCard";
import Spinner from "./components/Spinner";

const Halls = () => {
  const [loading, setLoading] = useState(false);
  const [occupancy, setOccupancy] = useState("");
  const [halls, setHalls] = useState([
    // {
    //   amenities: [],
    // },
  ]);

  const token = localStorage.getItem("token");

  const fetchHallType = async (occupancyValue) => {
    setLoading(true);
    try {
      const response = await axiosInstance.get("http://localhost:8000/api/viewhalls");
      setHalls(response.data);
    } catch (error) {
      console.error("Error fetching halls:", error);
    } finally {
      setLoading(false);
    }
  };

  const fetchHalls = async (occupancyValue) => {
    setLoading(true);
    try {
      const response = await axiosInstance.get(
        `http://localhost:8000/api/viewhalls?occupancy=${occupancyValue}`
      );

      const parsedOccupancyValue = parseInt(occupancyValue, 10);
      const filteredHalls = response.data.filter((hall) => hall.occupancy >= parsedOccupancyValue);
      
      setHalls(filteredHalls);
    } catch (error) {
      console.error("Error fetching halls:", error);
    } finally {
      setLoading(false);
    }
  };

  const handleSearchHall = (e) => {
    e.preventDefault();
    fetchHalls(occupancy);
  };

  useEffect(() => {
    if (occupancy) {
      fetchHalls(occupancy);
    } else {
      fetchHallType([]); // Reset halls data when the occupancy is empty
    }
  }, [occupancy]);



  // useEffect(() => {
  //   fetchHallType();
  // }, []);

  return (
    <div>
      <div className="">
        <div className="bg-indigo-900 h-72 bg-cover bg-center bg-[url('https://technext.github.io/royal/image/about_banner.jpg')] bg-blend-soft-light bg-fixed flex items-center justify-center">
          <div className="text-white ">
            {/* <div className="border-l-4 border-indigo-500 w-6/12 mx-auto"></div> */}
            <h2 className="text-6xl font-semibold tracking-wider border-l-8 border-indigo-500 pl-3 text-center flex items-center">
              <span>Halls</span>
            </h2>
          </div>
        </div>
        </div>
      {/* check availabiity form */}
        <div className="bg-white relative -top-16 py-5 px-4 md:mx-44 mx-5 md:rounded-full  rounded-lg border-4 border-indigo-300 ">
          <form className="space-y-3" onSubmit={handleSearchHall}>
            <div className="md:flex justify-center  md:gap-3">
              <div className="md:w-2/5 flex flex-col items-center mt-3">
              <label htmlFor="occupancy" className="text-bold text-indigo-700 text-lg">How many people are being invited??</label>
              </div>
              <div className="md:w-2/5 flex flex-col">
                <input
                  className="w-full h-12 bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200  text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"
                  type="number"
                  placeholder="How many people are being invited??"
                  name="occupancy"
                  id="occupancy"
                  onChange={(e) => setOccupancy(e.target.value)}
                  style={{ '::placeholder': { color: 'indigo' }}}
                />
              </div>
              {/* <div className="md:w-1/5 flex flex-col">
                <button
                  type="submit"
                  className="text-white h-12  bg-indigo-500 border-0 py-2 focus:outline-none hover:bg-indigo-600 rounded text-lg"
                >
                  Search Hall
                </button>
              </div> */}
            </div>
          </form>
        </div>

      <div className="my-8">
        <div>
          <section className="text-gray-600 body-font">
            <div className="container px-5 py-12 mx-auto">
              <div className="flex flex-wrap -m-4">
                {loading && <Spinner />}
                {!loading &&
                  halls.map((el, index) => {
                    return <HallCard key={index} {...el} index={index} />;
                  })}
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export default Halls;
